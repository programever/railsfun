class ApplicationController < ActionController::Base
	create_table :products do |t|
		t.string				:title
		t.text					:description
		t.timestamps
	end
end
